import React from 'react';
import './App.css';

class App extends React.Component {


    render() {

        return (
            <div className="container">
                <div>
                    <h1>Hello World !</h1>
                    <h3>Bienvenue sur mon premier projet React...</h3>
                </div>
            </div>
        );
    }
}

export default App;
